package com.angrymills.demomutithread;

import com.angrymills.demomutithread.domain.User;
import com.angrymills.demomutithread.service.DemoMutithreadService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoMutithreadApplication.class)
class DemoMutithreadApplicationTests {
    @Autowired
    DemoMutithreadService demoMutithreadService;
    @Test
    void contextLoads() {
    }
    /**
     * @Author gmwang
     * @Description new Thread 方式的主子线程 事务提交情况
     * @Date 2020/3/1 12:29
     * @Param []
     * @return void
     **/
    @Test
    public void test1() throws InterruptedException {
        demoMutithreadService.test1();
        Thread.sleep(200000);
    }
    @Test
    public void test2() throws InterruptedException {
        demoMutithreadService.test2();
        Thread.sleep(200000);
    }
}
