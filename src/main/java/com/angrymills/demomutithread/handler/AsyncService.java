package com.angrymills.demomutithread.handler;

import com.angrymills.demomutithread.dao.UserMapper;
import com.angrymills.demomutithread.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

/**
 * Created by gmwang on 2020/3/1
 */
@Service
public class AsyncService {
    @Autowired
    UserMapper userMapper;

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void test2(){
        User user = new User();
        user.setCreateTime(new Date());
        user.setName("testS1");
        user.setVersion(1);
        user.setUserNo(UUID.randomUUID().toString());
        userMapper.insert(user);
        System.out.println("子线程执行完成");
//        int i = 1 /0;
    }

}
