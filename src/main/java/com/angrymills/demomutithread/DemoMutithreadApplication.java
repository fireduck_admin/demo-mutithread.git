package com.angrymills.demomutithread;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.angrymills.demomutithread.dao")
public class DemoMutithreadApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMutithreadApplication.class, args);
    }

}
