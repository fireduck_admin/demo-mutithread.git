package com.angrymills.demomutithread.service;

import com.angrymills.demomutithread.dao.UserMapper;
import com.angrymills.demomutithread.domain.User;
import com.angrymills.demomutithread.handler.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

/**
 * Created by gmwang on 2020/3/1
 */
@Service
@Transactional
public class DemoMutithreadService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    AsyncService asyncService;

    @Transactional
    public void test1() throws InterruptedException {
        User user = new User();
        user.setCreateTime(new Date());
        user.setName("testM1");
        user.setVersion(1);
        user.setUserNo(UUID.randomUUID().toString());
        userMapper.insert(user);
        System.out.println("主线程执行完成");
        //断点打在这,看主线程执行完事务是否提交
        Thread.sleep(2000);

        new Thread(new Runnable() {
            @Override
            @Transactional
            public void run() {
                user.setCreateTime(new Date());
                user.setName("testS1");
                user.setVersion(1);
                user.setUserNo(UUID.randomUUID().toString());
                userMapper.insert(user);
                System.out.println("子线程执行完成");
                int i = 1 /0;
            }
        }).start();
    }
    @Transactional
    public void test2() throws InterruptedException {
        User user = new User();
        user.setCreateTime(new Date());
        user.setName("testM1");
        user.setVersion(1);
        user.setUserNo(UUID.randomUUID().toString());
        userMapper.insert(user);
        System.out.println("主线程执行完成");
        //断点打在这,看主线程执行完事务是否提交
        Thread.sleep(2000);
        asyncService.test2();
        int i = 1/0;
        Thread.sleep(2000);
    }
}
